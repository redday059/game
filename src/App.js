import React, { useState, useEffect, useRef } from 'react';
import './App.css';
import { utils } from './utils';

function Counter({onFinish, backFrom}) {
  const [count, setCount] = useState(backFrom);
  const [isRunning, setIsRunning] = useState(true);

  useInterval(() => {
    setCount(count - 1);
    if (count === 0) {
      setIsRunning(false);
      onFinish();
    }
  }, isRunning ? 1000 : null);

  return <h1>{count}</h1>;
}

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

// Color Theme
const colors = {
  available: 'lightgray',
  used: 'lightgreen',
  wrong: 'lightcoral',
  candidate: 'deepskyblue',
};

const Star = ({i}) => {
  return (
    <div className="star" />
  );
};

const DisplayStars = ({count}) => {
  return utils.range(1, count).map(i => <Star key={i} />)
};

const NumberButton = ({value, status, handleClick}) => {
  return (
    <button
      className="number"
      style={{'backgroundColor': colors[status]}}
      onClick={handleClick}
    >{value}
    </button>
  );
};

const SuggestNewGame = ({handleClick}) => {
  return <button onClick={handleClick}>Play again</button>
};

const StarMatch = () => {
  const [gameOver, setGameOver] = useState(false);
  const [stars, setStars] = useState(utils.random(1, 9));
  const [availableNums, setAvailableNums] = useState(utils.range(1, 9));
  // const [remaining, setRemaining] = useState(10);
  console.log('availableNums', availableNums);
  const [candidateNums, setCanditateNums] = useState([]);
  console.log('stars', stars);

  const launchNewGame = () => {
    setStars(utils.random(1, 9));
    setAvailableNums(utils.range(1, 9));
    setCanditateNums([]);
    setGameOver(false);
  };

  const calculateClassName = (i) => {
    if (availableNums.find((k) => k===i)) {
      return 'available'
    }
    if (candidateNums.find((k) => k===i)) {
      return utils.sum(candidateNums) <= stars ? 'candidate' : 'wrong'
    }
    return 'used';
  };

  useEffect(()=>{
    if (stars === 0) {
      return;
    }
    if (!candidateNums.length) {
      return;
    }
    if (utils.sum(candidateNums) === stars) {
      setStars(utils.randomSumIn(availableNums, 9));
      setCanditateNums([]);
    }
  },[ candidateNums, availableNums, stars ]);


  function filterOut(arr, val) {
    return arr.filter((i) => i !== val)
  }

  const handleNumberButtonClick = (num) => {
    // used
    if (!isAvailable(num) && !isCandidate(num)) {
      return;
    }
    // click back candidate
    if (isCandidate(num)) {
      setCanditateNums(filterOut(candidateNums,num));
      setAvailableNums([...availableNums, num]);
      console.log('isCand availableNums', availableNums);
      console.log('isCand candidateNums', candidateNums);
      return;
    }
    // add candidate
    setCanditateNums([...candidateNums, num]);
    setAvailableNums(filterOut(availableNums,num));
    console.log('availableNums?', availableNums);
    console.log('candidateNums?', candidateNums);
  };

  const isAvailable = (num) => {
    return availableNums.find(k => k===num);
  };

  const isCandidate = (num) => {
    return candidateNums.find(k => k===num);
  };



  const finishGame = () => setGameOver(true);

  return (
    <div className="game">
      <div className="help">
        Pick 1 or more numbers that sum to the number of stars
      </div>
      <div className="body">
        <div className="left">
          {stars !== 0 && !gameOver ? <DisplayStars count={stars} /> : <SuggestNewGame handleClick={launchNewGame}/> }
          {stars !== 0 && gameOver && (<div>Game Over!</div>)}
        </div>
        <div className="right">
          {utils.range(1,9).map((i) => (
            <NumberButton
              key={i}
              value={i}
              status={calculateClassName(i)}
              handleClick={() => handleNumberButtonClick(i)}
            />
          ))}
        </div>
      </div>
      {!gameOver && (
        <div className="timer">Time Remaining: <Counter backFrom={20} onFinish={finishGame}/></div>
      )}
    </div>
  );
};

function App() {
  return (
    <div className="App">
      <StarMatch />
    </div>
  );
}

export default App;
